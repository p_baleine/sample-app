var http = require('http'),
    config = require('config'),
    fs = require('fs'),

    body = '<html><head>' +
      '<link href="/application.css" media="all" rel="stylesheet" type="text/css"></head><body>' +
      '<h1>Hello, world</h1>' +
      '<p>The environmentSpecificValue is <strong>' +
      config.environmentSpecificValue +
      '</strong></p></body></html>',
    css = fs.readFileSync('public/application.css'),
    port = config.port;

http.createServer(function(req, res) {
  if (req.url === '/') {
    res.writeHead(200, {
      'Content-Length': body.length,
      'Content-Type': 'text/html'
    });
    res.end(body);

  } else if (req.url === '/application.css') {
    res.writeHead(200, {
      'Content-Length': css.length,
      'Content-Type': 'text/css'
    });
    res.end(css);

  } else {
    res.statusCode = 404;
    res.end('Not Found');
  }

}).listen(port);
